<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori_kost".
 *
 * @property int $id_kategori
 * @property string $nama_kategori
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class KategoriKost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategori_kost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kategori'], 'required'],
            [['created_time', 'modified_time'], 'safe'],
            [['nama_kategori'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Kategori',
            'nama_kategori' => 'Nama Kategori',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
