<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "durasi_sewa".
 *
 * @property int $id_durasi
 * @property string|null $durasi
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class DurasiSewa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'durasi_sewa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'modified_time'], 'safe'],
            [['durasi'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Durasi',
            'durasi' => 'Durasi',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
