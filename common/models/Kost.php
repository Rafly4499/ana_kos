<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kost".
 *
 * @property int $id_kost
 * @property int|null $id_tipe
 * @property int|null $id_kategori
 * @property int|null $id_kota
 * @property string|null $nama_kost
 * @property string|null $deskripsi
 * @property string|null $luas_kamar
 * @property string|null $fasilitas
 * @property string|null $harga
 * @property string|null $lokasi
 * @property string|null $gambar
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class Kost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipe', 'id_kategori', 'id_kota'], 'integer'],
            [['deskripsi', 'lokasi'], 'string'],
            [['created_time', 'modified_time'], 'safe'],
            [['nama_kost', 'fasilitas', 'harga'], 'string', 'max' => 100],
            [['luas_kamar'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kost' => 'Id Kost',
            'id_tipe' => 'Tipe Kos',
            'id_kategori' => 'Kategori Kos',
            'id_kota' => 'Kota',
            'nama_kost' => 'Nama Kost',
            'deskripsi' => 'Deskripsi',
            'luas_kamar' => 'Luas Kamar',
            'fasilitas' => 'Fasilitas',
            'harga' => 'Harga',
            'lokasi' => 'Lokasi',
            'gambar' => 'Gambar',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
     /**
     * @return \yii\db\ActiveQuery
     */
     public function getTipeKost()
     {
         return $this->hasOne(TipeKost::className(), ['id' => 'id_tipe']);
     }
 
     /**
      * @return \yii\db\ActiveQuery
      */
     public function getKategoriKost()
     {
         return $this->hasOne(KategoriKost::className(), ['id' => 'id_kategori']);
     }
 
     /**
      * @return \yii\db\ActiveQuery
      */
     public function getKota()
     {
         return $this->hasOne(Kota::className(), ['id' => 'id_kota']);
     }
}
