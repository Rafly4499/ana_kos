<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $id_booking
 * @property int|null $id_user
 * @property int|null $id_kost
 * @property int|null $id_penghuni
 * @property int|null $id_hitungan
 * @property int|null $id_durasi
 * @property string|null $tgl_masuk
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_kost', 'id_penghuni', 'id_hitungan', 'id_durasi','status'], 'integer'],
            [['tgl_masuk', 'created_time', 'modified_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Booking',
            'id_user' => 'User',
            'id_kost' => 'Kost',
            'id_penghuni' => 'Penghuni',
            'id_hitungan' => 'Hitungan Sewa',
            'id_durasi' => 'Durasi Sewa',
            'tgl_masuk' => 'Tgl Masuk',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
    public function getKost()
    {
        return $this->hasOne(Kost::className(), ['id' => 'id_kost']);
    }
    public function getPenghuni()
    {
        return $this->hasOne(Penghuni::className(), ['id' => 'id_penghuni']);
    }
    public function getHitunganSewa()
    {
        return $this->hasOne(HitunganSewa::className(), ['id' => 'id_hitungan']);
    }
    public function getDurasiSewa()
    {
        return $this->hasOne(DurasiSewa::className(), ['id' => 'id_durasi']);
    }
}
