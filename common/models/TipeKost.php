<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tipe_kost".
 *
 * @property int $id_tipe
 * @property string $nama_tipe
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class TipeKost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipe_kost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_tipe'], 'required'],
            [['created_time', 'modified_time'], 'safe'],
            [['nama_tipe'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Tipe',
            'nama_tipe' => 'Nama Tipe',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
