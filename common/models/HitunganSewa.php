<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hitungan_sewa".
 *
 * @property int $id_hitungan
 * @property string|null $hitungan_sewa
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class HitunganSewa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hitungan_sewa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time', 'modified_time'], 'safe'],
            [['hitungan_sewa'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Hitungan',
            'hitungan_sewa' => 'Hitungan Sewa',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
