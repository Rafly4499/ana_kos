<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kota".
 *
 * @property int $id_kota
 * @property string $nama_kota
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class Kota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kota'], 'required'],
            [['created_time', 'modified_time'], 'safe'],
            [['nama_kota'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kota' => 'Id Kota',
            'nama_kota' => 'Nama Kota',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
