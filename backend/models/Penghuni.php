<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "penghuni".
 *
 * @property int $id_penghuni
 * @property string|null $nama_lengkap
 * @property int|null $jenis_kelamin 1 Laki-laki; 2 Perempuan
 * @property string|null $no_hp
 * @property string|null $pekerjaan
 * @property string|null $deskripsi
 * @property string|null $created_time
 * @property string|null $modified_time
 */
class Penghuni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penghuni';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kelamin'], 'integer'],
            [['deskripsi'], 'string'],
            [['created_time', 'modified_time'], 'safe'],
            [['nama_lengkap', 'no_hp'], 'string', 'max' => 100],
            [['pekerjaan'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_penghuni' => 'Id Penghuni',
            'nama_lengkap' => 'Nama Lengkap',
            'jenis_kelamin' => 'Jenis Kelamin',
            'no_hp' => 'No Hp',
            'pekerjaan' => 'Pekerjaan',
            'deskripsi' => 'Deskripsi',
            'created_time' => 'Created Time',
            'modified_time' => 'Modified Time',
        ];
    }
}
