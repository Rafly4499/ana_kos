<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dominus77\sweetalert2\Alert;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bookings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_user',
                'value' => function($model){
                    if($model->user != NULL){
                    return $model->user->username;
                    }
                },
                'label' => 'User'
            ],
            [
                'attribute' => 'id_kost',
                'value' => function($model){
                    if($model->kost != NULL){
                    return $model->kost->nama_kost;
                    }
                },
                'label' => 'Kost'
            ],
            [
                'attribute' => 'id_penghuni',
                'value' => function($model){
                    if($model->penghuni != NULL){
                    return $model->penghuni->nama_lengkap;
                    }
                },
                'label' => 'Penghuni'
            ],
            [
                'attribute' => 'id_hitungan',
                'value' => function($model){
                    if($model->hitunganSewa != NULL){
                    return $model->hitunganSewa->hitungan_sewa;
                    }
                },
                'label' => 'Hitungan Sewa'
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 0){
                    return 'Belum Dikonfirmasi';
                    }else{
                    return 'Sudah Dikonfirmasi';
                    }
                },
                'label' => 'Status'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{confirm}',
                'contentOptions' => ['style'=>'text-align: center'],
                'buttons' => [
                    'confirm' => function($url, $model, $key){
                        if($model->status == 0){
                        return Html::a(Yii::t('app','{modelClass}',['modelClass'=>'Konfirmasi']),['booking/konfirm','id'=>$model->id], ['class' => 'btn btn-success modalButtonView']);
                        }
                    },
                    
                ],
            ],
            //'id_durasi',
            //'tgl_masuk',
            //'created_time',
            //'modified_time',

           
        ],
    ]); ?>


</div>
