<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DurasiSewaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Durasi Sewa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="durasi-sewa-index">

    <p>
        <?= Html::a('Create Durasi Sewa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'durasi',
           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
