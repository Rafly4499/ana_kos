<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DurasiSewa */

$this->title = 'Create Durasi Sewa';
$this->params['breadcrumbs'][] = ['label' => 'Durasi Sewa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="durasi-sewa-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
