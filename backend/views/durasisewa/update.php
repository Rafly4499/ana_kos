<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DurasiSewa */

$this->title = 'Update Durasi Sewa: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Durasi Sewa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="durasi-sewa-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
