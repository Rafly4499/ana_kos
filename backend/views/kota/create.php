<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kota */

$this->title = 'Create Kota';
$this->params['breadcrumbs'][] = ['label' => 'Kotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kota-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
