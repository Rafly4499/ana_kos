<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Kota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_kota')->textInput(['maxlength' => true]) ?>
    <?php if ($model->image == null) { ?>
				<?= $form->field($model, 'image')->widget(FileInput::classname(), [
					'options' => [
						'accept' => 'image/*',
						'required' => true,
					],
					'pluginOptions' => [
						// 'showPreview' => false,
						// 'showCaption' => true,
						// 'showRemove' => true,
						'removeClass' => 'btn btn-danger',
						'showUpload' => false,
						'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>'
					],
				]) ?>
			<?php } else { ?>
				<?php
				echo FileInput::widget([
					'model' => $model,
					'attribute' => 'image',
					'options' => [
						'accept' => 'image/*'
					],
					'pluginOptions' => [
						'initialPreview' => [
							Html::img(Yii::$app->urlManager->createUrl(['images/' . $model->image]), ['style' => 'width:150px;']),
						],
						'removeClass' => 'btn btn-danger',
						'showUpload' => false,
						'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
						'overwriteInitial' => false,
					]
				]);
				?><br>
			<?php } ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
