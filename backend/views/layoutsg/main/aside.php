<?php
  use yii\helpers\Url;
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?=$directoryAsset?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
      <p> <?=Yii::$app->user->identity->username?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'fa fa-folder-open', 'url' => ['/dashboard']],
                    [
                        'label' => 'Data Kos',
                        'icon' => 'fa fa-users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Kategori Kos', 'icon' => 'fa fa-reorder', 'url' => ['/kategorikost/index']],//this is ok
                            ['label' => 'Tipe Kos', 'icon' => 'fa fa-cog', 'url' => ['/tipekost/index']],//this is ok
                            ['label' => 'Data Kos', 'icon' => 'fa fa-cogs', 'url' => ['/kost/index']],//this is ok
                        ],
                    ],
                    // ['label' => 'News categories', 'icon' => 'fa fa-folder-open', 'url' => ['/news-category']],
                    // [
                    //     'label' => 'Posts',
                    //     'icon' => 'fa fa-archive',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Posts', 'icon' => 'fa fa-reorder', 'url' => ['/posts'],],
                    //         ['label' => 'New post', 'icon' => 'fa fa-plus', 'url' => ['/posts/create'],],
                    //     ],
                    // ],
                    // ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>
    
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>