<?php
use yii\bootstrap\Nav;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu Ana_Kos', 'options' => ['class' => 'header']],
                    ['label' => 'Dashboard', 'icon' => 'fa fa-home', 'url' => ['/site/index']],
                    [
                        'label' => 'Data Kos',
                        'icon' => 'fa fa-users',
                        'url' => '#',   
                        'items' => [
                            ['label' => 'Kategori Kos', 'icon' => 'fa fa-reorder', 'url' => ['/kategorikost/index'],],//this is ok
                            ['label' => 'Tipe Kos', 'icon' => 'fa fa-cog', 'url' => ['/tipekost/index'],],//this is ok
                            ['label' => 'Data Kos', 'icon' => 'fa fa-cogs', 'url' => ['/kost/index'],],//this is ok
                            
                        ],
                    ],
                    
                    [
                        'label' => 'Sewa Kos',
                        'icon' => 'fa fa-archive',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Durasi Sewa', 'icon' => 'fa fa-reorder', 'url' => ['/durasisewa/index'],],
                            ['label' => 'Hitungan Sewa', 'icon' => 'fa fa-plus', 'url' => ['/hitungansewa/index'],],
                        ],
                    ],
                    ['label' => 'Kota', 'icon' => 'fa fa-folder-open', 'url' => ['/kota/index']],
                    ['label' => 'Penghuni', 'icon' => 'fa fa-file-code-o', 'url' => ['/penghuni/index']],
                    ['label' => 'Booking', 'icon' => 'fa fa-dashboard', 'url' => ['/booking/index']],
                    ['label' => 'Manage User', 'icon' => 'fa fa-dashboard', 'url' => ['/user/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

       

    </section>

</aside>
