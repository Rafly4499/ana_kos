<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kost */

$this->title = 'Create Kost';
$this->params['breadcrumbs'][] = ['label' => 'Kost', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kost-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
