<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\money\MaskMoney;
use yii\helpers\ArrayHelper;
use common\models\TipeKost;
use common\models\KategoriKost;
use common\models\Kota;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\Kost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kost-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_tipe')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TipeKost::find()->all(), 'id', 'nama_tipe'),
        'language' => 'en',
        'options' => ['placeholder' => 'Pilih Tipe Kos ...', 'required'=>true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Pilih Tipe Kost');

    ?>
    <?= $form->field($model, 'id_kategori')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori'),
        'language' => 'en',
        'options' => ['placeholder' => 'Pilih Kategori Kos ...', 'required'=>true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Pilih Kategori Kost');

    ?>
    <?= $form->field($model, 'id_kota')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota'),
        'language' => 'en',
        'options' => ['placeholder' => 'Pilih Kota ...', 'required'=>true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Pilih Kota');

    ?>
    
    <?= $form->field($model, 'nama_kost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'luas_kamar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fasilitas')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'harga')->widget(
        MaskMoney::classname(),
        [
            'options' => [
                'required' => true
            ],
            'pluginOptions' => [
                'prefix' => 'Rp. ',
                'suffix' => '',
                'affixesStay' => true,
                'thousands' => '.',
                'decimal' => ',',
                'precision' => 0,
                'allowZero' => false,
                'allowNegative' => false,
            ]
        ]
    )->label('Harga') ?>

    <?= $form->field($model, 'lokasi')->textarea(['rows' => 6]) ?>
    <?php if ($model->gambar == null) { ?>
				<?= $form->field($model, 'gambar')->widget(FileInput::classname(), [
					'options' => [
						'accept' => 'image/*',
						'required' => true,
					],
					'pluginOptions' => [
						// 'showPreview' => false,
						// 'showCaption' => true,
						// 'showRemove' => true,
						'removeClass' => 'btn btn-danger',
						'showUpload' => false,
						'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>'
					],
				]) ?>
			<?php } else { ?>
				<?php
				echo FileInput::widget([
					'model' => $model,
					'attribute' => 'gambar',
					'options' => [
						'accept' => 'image/*'
					],
					'pluginOptions' => [
						'initialPreview' => [
							Html::img(Yii::$app->urlManager->createUrl(['images/' . $model->gambar]), ['style' => 'width:150px;']),
						],
						'removeClass' => 'btn btn-danger',
						'showUpload' => false,
						'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
						'overwriteInitial' => false,
					]
				]);
				?><br>
			<?php } ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
