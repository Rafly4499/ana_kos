<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kost';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kost-index">

    <p>
        <?= Html::a('Create Data Kost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_kost',
            [
                'attribute' => 'id_tipe',
                'value' => function($model){
                    if($model->tipeKost != NULL){
                    return $model->tipeKost->nama_tipe;
                    }
                },
                'label' => 'Tipe Kos'
            ],
            [
                'attribute' => 'id_kategori',
                'value' => function($model){
                    if($model->kategoriKost != NULL){
                    return $model->kategoriKost->nama_kategori;
                    }
                },
                'label' => 'Kategori Kos'
            ],
            [
                'attribute' => 'id_kota',
                'value' => function($model){
                    if($model->kota != NULL){
                    return $model->kota->nama_kota;
                    }
                },
                'label' => 'Kota'
            ],
            'nama_kost',
            // 'deskripsi:ntext',
            'luas_kamar',
            // 'fasilitas',
            'harga',
            [
                'attribute' => 'gambar',
                'format' => 'html',
                'value'=>function($data){
                    return Html::img('../../web/images/'.$data['gambar'],['width' => '150px']);
                }
            ],
            // 'lokasi:ntext',
            //'created_time',
            //'modified_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
