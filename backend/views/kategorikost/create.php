<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KategoriKost */

$this->title = 'Create Kategori Kos';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Kos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-kost-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
