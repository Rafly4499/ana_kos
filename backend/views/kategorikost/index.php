<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KategoriKostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori Kos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-kost-index">

    <p>
        <?= Html::a('Create Kategori Kos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_kategori',
            'nama_kategori',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
