<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KategoriKost */

$this->title = 'Update Kategori Kost: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Kost', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kategori-kost-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
