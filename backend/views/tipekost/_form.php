<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TipeKost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipe-kost-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_tipe')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'created_time')->textInput() ?>

    <?= $form->field($model, 'modified_time')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
