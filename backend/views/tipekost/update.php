<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TipeKost */

$this->title = 'Update Tipe Kost: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tipe Kost', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipe-kost-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
