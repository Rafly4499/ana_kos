<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TipeKost */

$this->title = 'Create Tipe Kost';
$this->params['breadcrumbs'][] = ['label' => 'Tipe Kost', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipe-kost-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
