<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TipeKostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipe Kost';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipe-kost-index">

    <p>
        <?= Html::a('Create Tipe Kost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_tipe',
            'nama_tipe',
            // 'created_time',
            // 'modified_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
