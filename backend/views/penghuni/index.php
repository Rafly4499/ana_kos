<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PenghuniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penghuni';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penghuni-index">    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

       
            'nama_lengkap',
            [
                'attribute' => 'jenis_kelamin',
                'value' => function($model){
                    if($model->jenis_kelamin != 1){
                    return 'Perempuan';
                    }else{
                        return 'Laki - Laki';
                    }
                },
                'label' => 'Jenis Kelamin'
            ],
            
            'no_hp',
            'pekerjaan',
            //'deskripsi:ntext',
            //'created_time',
            //'modified_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
