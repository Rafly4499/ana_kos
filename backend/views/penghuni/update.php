<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Penghuni */

$this->title = 'Update Penghuni: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Penghuni', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penghuni-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
