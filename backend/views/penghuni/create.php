<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Penghuni */

$this->title = 'Create Penghuni';
$this->params['breadcrumbs'][] = ['label' => 'Penghunis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penghuni-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
