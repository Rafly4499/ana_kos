<?php

/* @var $this yii\web\View */
use dosamigos\chartjs\ChartJs;
$this->title = 'Ana_Kosts';
?>
<div class="site-index">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Penghuni</span>
                        <span class="info-box-number"><?= $sumpenghuni ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Pemilik</span>
                        <span class="info-box-number"><?= $sumuser ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-list-alt"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Booking</span>
                        <span class="info-box-number"><?= $sumbooking ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Kost</span>
                        <span class="info-box-number"><?= $sumkost ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <!-- /.col -->
        </div>
    </div>
    
        <div class="row">
            <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Grafik Booking</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <div class="box-body">
                <?= ChartJs::widget([
                    'type' => 'bar',
                    'options' => [
                        'height' => 100,
                        'title' => 'Grafik Booking Kosts',
                    ],
                    'data' => [
                        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                        'datasets' => [
                            [
                                'label' => "Grafik Booking",
                                'backgroundColor' => "rgba(179,181,198,0.2)",
                                'borderColor' => "rgba(179,181,198,1)",
                                'pointBackgroundColor' => "rgba(179,181,198,1)",
                                'pointBorderColor' => "#fff",
                                'pointHoverBackgroundColor' => "#fff",
                                'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                'data' => [1, 3, 5, 1, 2, 1, 4]
                            ],
                    ]
                        ],
                ]);
                ?>
            </div>
                    </div>
                </div>
                </div>
        <div class="col-md-4">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-archive"></i></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tipe Kos Putra</span>
                    <span class="info-box-number"><?= $tipekostputra ?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        Total Tipe Kos Putra
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-archive"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tipe Kos Putri</span>
                    <span class="info-box-number"><?= $tipekostputri ?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                        Total Tipe Kos Putri
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-archive"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tipe Kos Campur</span>
                    <span class="info-box-number"><?= $tipekostcampur?></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                    <span class="progress-description">
                        Total Tipe Kos Campur
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

    </div>
    <div class="row">
    <div class="col-md-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Grafik Penghuni</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <div class="box-body">
                <?= ChartJs::widget([
                    'type' => 'bar',
                    'options' => [
                        'height' => 100,
                        'title' => 'Grafik Penghuni',
                    ],
                    'data' => [
                        'labels' => ["January", "February", "March", "April", "May", "June", "July"],
                        'datasets' => [
                            [
                                'label' => "Grafik Penghuni<",
                                'backgroundColor' => "rgba(179,181,198,0.2)",
                                'borderColor' => "rgba(179,181,198,1)",
                                'pointBackgroundColor' => "rgba(179,181,198,1)",
                                'pointBorderColor' => "#fff",
                                'pointHoverBackgroundColor' => "#fff",
                                'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                'data' => [3, 9, 6,3, 5, 20, 15]
                            ],
                    ]
                        ],
                ]);
                ?>
            </div>
                    </div>
                </div>
                </div>
    </div>

</div>