<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HitunganSewaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hitungan Sewas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hitungan-sewa-index">

    <p>
        <?= Html::a('Create Hitungan Sewa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'hitungan_sewa',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
