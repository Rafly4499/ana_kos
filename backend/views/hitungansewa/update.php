<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HitunganSewa */

$this->title = 'Update Hitungan Sewa: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hitungan Sewa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hitungan-sewa-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
