<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HitunganSewa */

$this->title = 'Create Hitungan Sewa';
$this->params['breadcrumbs'][] = ['label' => 'Hitungan ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hitungan-sewa-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
