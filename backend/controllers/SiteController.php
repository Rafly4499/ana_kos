<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\Penghuni;
use common\models\Booking;
use common\models\Kost;
use common\models\TipeKost;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $sumpenghuni = Penghuni::find()->count();
        $sumuser = User::find()->count();
        $sumbooking = Booking::find()->count();
        $sumkost = Kost::find()->count();
        $tipekost = TipeKost::find();
        $tipekostputra = Kost::find()->where(['id_tipe' => 1])->count();
        $tipekostputri = Kost::find()->where(['id_tipe' => 2])->count();
        $tipekostcampur = Kost::find()->where(['id_tipe' => 3])->count();
        return $this->render('index',[
            'sumpenghuni' => $sumpenghuni,
            'sumuser' => $sumuser,
            'sumbooking' => $sumbooking,
            'sumkost' => $sumkost,
            'tipekostcampur' => $tipekostcampur,
            'tipekostputri' => $tipekostputri,
            'tipekostputra' => $tipekostputra,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
