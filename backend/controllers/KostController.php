<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Kost;
use common\models\KostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * KostController implements the CRUD actions for Kost model.
 */
class KostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kost models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kost model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kost model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        date_default_timezone_set("Asia/Jakarta");
        $model = new Kost();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = \Yii::$app->db->beginTransaction();
            
                        try {
                            
                            //upload foto
                            $images = UploadedFile::getInstance($model, 'gambar');
                            $images_name = 'image-' . $model->id . '-' . time() . '.' . $images->extension;
                            $path = 'images/' . $images_name;
                            if ($images->saveAs($path)) {
                                $model->gambar = $images_name;
                            }
                            $model->created_time = date('Y-m-d H:i:s');
                            $model->save(false);
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', "Tambah Data Kos Berhasil");
                            return $this->redirect(['index']);
                        } catch (\Exception $e) {
                            
                                            $transaction->rollback();
                                            Yii::$app->session->setFlash('error', $e->getMessage());
                                            return $this->redirect(['index']);
                                        }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Kost model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        date_default_timezone_set("Asia/Jakarta");
        $model = $this->findModel($id);
        $oldPhoto = $model->gambar;
        if ($model->load(Yii::$app->request->post())) {
            if ($images = UploadedFile::getInstance($model, 'gambar') == null) {
                $model->gambar = $oldPhoto;
            } else {

                //upload foto
                $images = UploadedFile::getInstance($model, 'gambar');
                $images_name = 'gambar-' . $model->id . '-' . time() . '.' . $images->extension;
                $path = 'images/' . $images_name;
                if ($images->saveAs($path)) {
                    $model->gambar = $images_name;
                }
            }

            $model->modified_time = date('Y-m-d H:i:s');
            $model->save(false);
            Yii::$app->session->setFlash('success', "Update Data Kos Berhasil");
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
