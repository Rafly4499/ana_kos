<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\ArrayHelper;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Kost;
use common\models\Kota;
use common\models\TipeKost;
use common\models\KategoriKost;
use common\models\HitunganSewa;
use common\models\DurasiSewa;
use common\models\Booking;
use common\models\Penghuni;
use common\models\User;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {       
        $this->layout = 'main.php';
        $kota = Kota::find()->limit(8)->orderBy('id DESC')->all();
        $kost = Kost::find()->limit(6)->orderBy('id DESC')->all();
        $datalist = ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori');
        $datalistkota = ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota');
        return $this->render('index',[
            'kost' => $kost,
            'kota' => $kota,
            'datalist' => $datalist,
            'datalistkota' => $datalistkota
        ]);
    }
    public function actionKostsearch()
    {
        $name = Yii::$app->request->get('qsearch');
        $kategori = Yii::$app->request->get('nama_kategori');
        $kota = Yii::$app->request->get('nama_kota');
        $kost = Kost::find()->where(['LIKE', 'nama_kost', $name])
            ->andWhere(['LIKE', 'id_kategori', $kategori])
            ->andWhere(['LIKE', 'id_kota', $kota])
            ->all();
        $datalist = ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori');
        $datalistkota = ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota');
        //var_dump($models);
        return $this->render('kost', [
            'kost' => $kost,
            'datalist' => $datalist,
            'datalistkota' => $datalistkota,
           //  'dataProvider' => $dataProvider
            
    
       ]);
    }  
    /**
     * Displays kost.
     *
     * @return mixed
     */
     public function actionKost()
     {       
 
         $kota = Kota::find()->limit(8)->orderBy('id DESC')->all();
         $kost = Kost::find()->orderBy('id DESC')->all();
         $datalist = ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori');
         $datalistkota = ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota');
         return $this->render('kost',[
             'kost' => $kost,
             'kota' => $kota,
             'datalist' => $datalist,
             'datalistkota' => $datalistkota
         ]);
     }
      /**
     * Displays detailkost.
     *
     * @return mixed
     */
     public function actionDetailkost($id)
     {       
        $model = $this->findModel($id);
         $kota = Kota::find()->limit(8)->orderBy('id DESC')->all();
         $tipe = TipeKost::find()->all();
         $kategori = KategoriKost::find()->all();
         $detailkos = Kost::findOne($id);

         return $this->render('detailkost',[
             'detailkos' => $detailkos,
             'kota' => $kota,
             'kategori' => $kategori,
             'tipe' => $tipe
         ]);
     }

     /**
     * Displays kota.
     *
     * @return mixed
     */
     public function actionArtikel()
     {       

         $kota = Kota::find()->orderBy('id DESC')->all();
         $kost = Kost::find()->orderBy('id DESC')->all();
         return $this->render('artikel',[
             'kost' => $kost,
             'kota' => $kota
         ]);
     }
     /**
     * Displays kota.
     *
     * @return mixed
     */
     public function actionProfil($id)
     {       
        $histori = Booking::find()->where(['id_user'=>$id])->all();
         $historipenghuni = Booking::find()->where(['id_user'=>$id])->one();
         $penghuni = Penghuni::find()->where(['id' => $historipenghuni->id_penghuni])->one();
         $profile = User::findOne($id);
         return $this->render('profil',[
             'profile' => $profile,
             'historipenghuni' => $historipenghuni,
             'penghuni' => $penghuni,
             'histori' => $histori
         ]);
     }
     public function actionFasilitas($id)
     {       
        $kota = Kota::find()->limit(8)->orderBy('id DESC')->all();
        $kost = Kost::find()->where(['id_kategori'=>$id])->all();
        $datalist = ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori');
        $datalistkota = ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota');
        return $this->render('kost',[
            'kost' => $kost,
            'kota' => $kota,
            'datalist' => $datalist,
            'datalistkota' => $datalistkota
        ]);
     }
     public function actionKota($id)
     {       
        $kota = Kota::find()->limit(8)->orderBy('id DESC')->all();
        $kost = Kost::find()->where(['id_kota'=>$id])->all();
        $datalist = ArrayHelper::map(KategoriKost::find()->all(), 'id', 'nama_kategori');
        $datalistkota = ArrayHelper::map(Kota::find()->all(), 'id', 'nama_kota');
        return $this->render('kost',[
            'kost' => $kost,
            'kota' => $kota,
            'datalist' => $datalist,
            'datalistkota' => $datalistkota
        ]);
     }
      /**
     * Displays kota.
     *
     * @return mixed
     */
     public function actionBooking($id)
     {  
        if(!Yii::$app->user->isGuest){
        $cekuser = Yii::$app->user->identity->id;
        $sewa = HitunganSewa::find()->all();
        $durasi = DurasiSewa::find()->all();
        $penghuni = new Penghuni();
        $booking = new Booking();
       
                if ($penghuni->load(Yii::$app->request->post()) && $booking->load(Yii::$app->request->post())) {
                    $transaction = Yii::$app->db->beginTransaction();
                    $hasilpenghuni = $penghuni->save();
                    if($hasilpenghuni){
                        $modelBooking = new Booking();
                        $modelBooking->id_user =  $cekuser;
                        $modelBooking->id_kost =  $id;
                        $modelBooking->id_penghuni =  $penghuni->id;
                        $modelBooking->id_hitungan =  $booking->id_hitungan;
                        $modelBooking->id_durasi =  $booking->id_durasi;
                        $date = date_create($booking->tgl_masuk);
                        $dateFix = date_format($date, 'Y-m-d');
                        $modelBooking->tgl_masuk = $dateFix;
                        $modelBooking->status = 0;
                        $resultbooking = $modelBooking->save(false);
                        $transaction->commit();
                        
                        Yii::$app->session->setFlash(\dominus77\sweetalert2\Alert::TYPE_SUCCESS, [
                           [
                               'title' => 'Selamat Anda Telah Berhasil Booking!!!',
                               'text' => 'Silahkan menunggu verifikasi booking Anda',
                               'confirmButtonText' => 'Done!',
                           ]
                        ]);
                        return $this->redirect(['site/index']);
                    }
                    }
        
                return $this->render('booking', [
                    'penghuni' => $penghuni,
                    'booking' => $booking,
                    'sewa' => $sewa,
                    'durasi' => $durasi,
                ]);
     }else{
        Yii::$app->session->setFlash('error', 'Anda harus Login Terlebih Dahulu');
     }
    }
     /**
     * Displays kota.
     *
     * @return mixed
     */
     public function actionKonfirmBooking($id)
     {          
        $sewa = HitunganSewa::find()->all();
        $durasi = DurasiSewa::find()->all();
        $penghuni = new Penghuni();
        $booking = new Booking();
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
        
                return $this->render('booking', [
                    'model' => $model,
                    'model' => $sewa,
                    'model' => $durasi,
                ]);
     }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
    protected function findModel($id)
    {
        if (($model = Kost::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
