<?php
use frontend\assets\AppAsset;
use yii\helpers\Url;
use common\models\User;
use yii\helpers\Html;
use dominus77\sweetalert2\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Ana Kos</title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
  </head>
  <body>
  <?php $this->beginBody() ?>
  <?= \dominus77\sweetalert2\Alert::widget(['useSessionFlash' => true]) ?>
  <?php
    if (Yii::$app->session->hasFlash('success')) {
        ?>
        <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4>Saved !!!</h4>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php
    } else if (Yii::$app->session->hasFlash('error')) {
        ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Warning !!!</strong> <?= Yii::$app->session->getFlash('error') ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

    <?php
    }
    ?>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="<?= Url::toRoute(['site/index']); ?>"><?= Html::img('@web/images/logoana.png', ['alt' => '', 'style' => 'width:200px;']); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="<?= Url::toRoute(['site/index']); ?>" class="nav-link">Beranda</a></li>
            
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="room.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Layanan</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="room.html">Pencarian Kos</a>
                  <a class="dropdown-item" href="room.html">Pencarian Apartemen</a>
                  <a class="dropdown-item" href="room.html">Premier Suite</a>
                  <a class="dropdown-item" href="room.html">Family Room</a>
                </div>
            </li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/kost']); ?>" class="nav-link">Layanan Kos</a></li>
            <!-- <li class="nav-item"><a href="amenities.html" class="nav-link">Pencarian Kos</a></li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/artikel']); ?>" class="nav-link">Artikel</a></li>
            <!-- <li class="nav-item"><a href="blog.html" class="nav-link">Kontak Kami</a></li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/about']); ?>" class="nav-link">Tentang Kami</a></li>
            
          
          <?php
                    if (Yii::$app->user->isGuest) {
                        ?><li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="room.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Masuk</a>
                          <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <a class="dropdown-item" href="<?= Url::toRoute(['site/login']); ?>">Pencari</a>
                            <a class="dropdown-item" href="<?= Url::toRoute(['site/login']); ?>">Pemilik</a>
                          </div>
                      </li>
                        
                    <?php
                    } else {
                        ?>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="room.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Halo, <?= Yii::$app->user->identity->username ?></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown04">
                            <?php
                                        $cekmember = Yii::$app->user->identity->id;
                                        $member = User::find()->where(['id' => $cekmember])->one();
                                        ?>
                              <a class="dropdown-item" href="<?= Url::toRoute(['site/profil', 'id' => $cekmember]); ?>">Profil</a>
                              <a class="dropdown-item">
                              <?= Html::a(
                                            'Logout',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?></a>
                              
                            </div>
                        </li>
                        </ul>
                        <?php
                    }
                    ?>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    <main>
        <?= $content ?>
    </main>
    
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Ana Kos</h2>
              <p>Platform penyedia informasi dan booking Kos berbasis aplikasi dan kemitraan</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Link Halaman</h2>
              <ul class="list-unstyled">
                <li><a href="<?= Url::toRoute(['site/index']); ?>" class="py-2 d-block">Beranda</a></li>
                <li><a href="<?= Url::toRoute(['site/kos']); ?>" class="py-2 d-block">Layanan Kos</a></li>
                <li><a href="<?= Url::toRoute(['site/artikel']); ?>" class="py-2 d-block">Artikel</a></li>
                <li><a href="<?= Url::toRoute(['site/tentangkami']); ?>" class="py-2 d-block">Tentang Kami</a></li>
                
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Informasi Kami</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Kampus ITS, Jl. Raya ITS, Keputih, Kec. Sukolilo, Kota SBY, Jawa Timur 60111</a></li>
                <li><a href="#" class="py-2 d-block">(031) 5947280</a></li>
                <li><a href="#" class="py-2 d-block">anakost@gmail.com</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Kontak Kami</h2>
              <p>Subscribe kami untuk mendapatkan pemberitahuan dari kami.</p>
              <form action="#" class="subscribe-form">
                <div class="form-group">
                  <span class="icon icon-paper-plane"></span>
                  <input type="text" class="form-control" placeholder="Subscribe">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <i class="icon-heart" aria-hidden="true"></i> by Ana Kos
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>