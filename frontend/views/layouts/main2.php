<?php
use frontend\assets\AppAsset;
use yii\helpers\Url;
use common\models\User;
use yii\helpers\Html;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Ana Kos</title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
  <?php $this->beginBody() ?>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="<?= Url::toRoute(['site/index']); ?>"><img style="width:200px;"src="images/logoana.png"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="<?= Url::toRoute(['site/index']); ?>" class="nav-link">Beranda</a></li>
            
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="room.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Layanan</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="room.html">Pencarian Kos</a>
                  <a class="dropdown-item" href="room.html">Pencarian Apartemen</a>
                  <a class="dropdown-item" href="room.html">Premier Suite</a>
                  <a class="dropdown-item" href="room.html">Family Room</a>
                </div>
            </li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/kost']); ?>" class="nav-link">Layanan Kos</a></li>
            <!-- <li class="nav-item"><a href="amenities.html" class="nav-link">Pencarian Kos</a></li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/artikel']); ?>" class="nav-link">Artikel</a></li>
            <!-- <li class="nav-item"><a href="blog.html" class="nav-link">Kontak Kami</a></li> -->
            <li class="nav-item"><a href="<?= Url::toRoute(['site/tentangkami']); ?>" class="nav-link">Tentang Kami</a></li>
            <li class="nav-item"><a href="<?= Url::toRoute(['site/login']); ?>" class="nav-link">Login</a></li>
          </ul>
          <?php
                    if (Yii::$app->user->isGuest) {
                        ?>
                        <li class="nav-item"><a href="<?= Url::toRoute(['site/login']); ?>" class="nav-link">Login</a></li>
                    <?php
                    } else {
                        ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-lg logstatus">Halo, <?= Yii::$app->user->identity->username ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?php
                                        $ceklog = Yii::$app->user->identity->id;
                                        $studen = User::find()->where(['id' => $ceklog])->one();
                                        ?>
                                        <a href="<?= Url::toRoute(['site/profil']); ?>" class="btn btn-default btn-flat">Profil</a>
                                    </div>
                                    <div class="pull-left">
                                        <?= Html::a(
                                            'Logout',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    <main>
        <?= $content ?>
    </main>
    
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Ana Kos</h2>
              <p>Platform penyedia informasi dan booking Kos berbasis aplikasi dan kemitraan</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Link Halaman</h2>
              <ul class="list-unstyled">
                <li><a href="<?= Url::toRoute(['site/index']); ?>" class="py-2 d-block">Beranda</a></li>
                <li><a href="<?= Url::toRoute(['site/kos']); ?>" class="py-2 d-block">Layanan Kos</a></li>
                <li><a href="<?= Url::toRoute(['site/artikel']); ?>" class="py-2 d-block">Artikel</a></li>
                <li><a href="<?= Url::toRoute(['site/tentangkami']); ?>" class="py-2 d-block">Tentang Kami</a></li>
                
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Informasi Kami</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Kampus ITS, Jl. Raya ITS, Keputih, Kec. Sukolilo, Kota SBY, Jawa Timur 60111</a></li>
                <li><a href="#" class="py-2 d-block">(031) 5947280</a></li>
                <li><a href="#" class="py-2 d-block">anakost@gmail.com</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Kontak Kami</h2>
              <p>Subscribe kami untuk mendapatkan pemberitahuan dari kami.</p>
              <form action="#" class="subscribe-form">
                <div class="form-group">
                  <span class="icon icon-paper-plane"></span>
                  <input type="text" class="form-control" placeholder="Subscribe">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <i class="icon-heart" aria-hidden="true"></i> by Ana Kos
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>