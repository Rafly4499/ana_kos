<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
?>
<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span class="mr-2"><a href="#">Kos</a></span> <span>Detail Kos</span></p>
              <h1 class="mb-3">Detail Kos</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 ftco-animate">
            <h2 class="mb-3"><?= $detailkos->nama_kost ?></h2>
            <p>Untuk <?= $detailkos->tipeKost->nama_tipe ?></p>
            <p>
            <a href="" class="img-fluid"><?= Html::img(Yii::$app->urlManagerBackend->baseUrl . '/images/'.$detailkos->gambar) ?></a><br>
            </p>
            <h6>Tipe Tempat Tinggal</h6>
            <h5><?= $detailkos->kategoriKost->nama_kategori ?></h5><br>
          
           <h6>Luas Kamar</h6>
           <h5><?= $detailkos->luas_kamar ?></h5><br>
            
            <h6>Deskripsi</h6>
            <h5><?= $detailkos->deskripsi ?></h5><br>
            <h6>Deskripsi</h6>
            <h5><?= $detailkos->fasilitas ?></h5><br>
           <h6>Lokasi Alamat Kos</h6>
           <h5><?= $detailkos->lokasi ?></h5><br>
            
            <div class="pt-5 mt-5">
              
              
              <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">Tinggalkan Komentar</h3>
                <form action="#" class="p-5 bg-light">
                  <div class="form-group">
                    <label for="name">Nama *</label>
                    <input type="text" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="email" class="form-control" id="email">
                  </div>

                  <div class="form-group">
                    <label for="message">Pesan</label>
                    <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
                  </div>

                </form>
              </div>
            </div>

          </div> <!-- .col-md-8 -->
          <div class="col-md-4 sidebar">
          <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Booking</h3>
                <p>Data bisa berubah sewaktu-waktu</p>
                <li><p>Harga : <h4>Rp. <?= $detailkos->harga ?>,-</h4></p><br></li>
                <li>Tidak Termasuk Listrik</li>
                <li>Tidak ada min.pembayaran<br></li>
                <?php
                if(Yii::$app->user->isGuest){
                ?>
                <a href="#"><button class="btn btn-primary py-2 px-4" data-toggle="modal" data-target="#bookingModalCenter">Booking</button></a> | <a href="#"><button class="btn btn-primary py-2 px-4" data-toggle="modal" data-target="#exampleModalCenter">Hubungi Kost</button></a>
                <?php
                }else{
                ?>
                <a href="<?= Url::toRoute(['site/booking', 'id' => $detailkos->id]); ?>"><button class="btn btn-primary py-2 px-4">Booking</button></a> | <a href="#"><button class="btn btn-primary py-2 px-4" data-toggle="modal" data-target="#exampleModalCenter">Hubungi Kost</button></a>
                <?php
                }
                ?>
              </div>
            </div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Hubungi Kost</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <p>Mohon maaf saat ini pemilik kost sedang offline. Maka dari itu silahkan menghubungi nomor yang ada dibawah ini</p><br>
         <p>Pemilik Kost : 085732424234 <br>
         Admin : 082290037810</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="bookingModalCenter" tabindex="-1" role="dialog" aria-labelledby="bookingModalCenter" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-contentbooking">
      <div class="modal-header">
        <h5 class="modal-title" id="bookingModalCenter">Warning</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <p>Mohon Maaf Anda Harus Login Terlebih Dahulu</p>
      </div>
      <div class="modal-footer">
        <a href="<?= Url::toRoute(['site/login']); ?>"><button type="button" class="btn btn-primary">Login</button></a>
      </div>
    </div>
  </div>
</div>     
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Kategori</h3>
                <?php
                    if ($kategori) {
                    foreach ($kategori as $kategorikos => $resultkategori) {

                    ?>
                <li><a href="#"><?= $resultkategori->nama_kategori ?> <span>~</span></a></li>
               <?php
                    }
                }
               ?>
              </div>
            </div>
            <div class="sidebar-box ftco-animate">
              <div class="categories">
                <h3>Tipe Kos</h3>
                <?php
                    if ($tipe) {
                    foreach ($tipe as $tipekos => $resulttipe) {

                    ?>
                <li><a href="#"><?= $resulttipe->nama_tipe ?> <span>~</span></a></li>
               <?php
                    }
                }
               ?>
               
              </div>
            </div>

            

            <div class="sidebar-box ftco-animate">
              <h3>Ana Kos</h3>
              <p>Platform digital penyedia Informasi dan Booking tempat tinggal bagi Anak Kost</p>
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->