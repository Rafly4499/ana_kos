<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
  use yii\bootstrap\ActiveForm;
  use dosamigos\datepicker\DatePicker;
  use kartik\select2\Select2;
  use yii\helpers\ArrayHelper;
  use common\models\HitunganSewa;
  use common\models\DurasiSewa;
  
?>
<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Booking</span></p>
              <h1 class="mb-3">Booking</h1>
            </div>
          </div>
        </div>
      </div>
    </section>


    
    <section class="ftco-section">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-12 ftco-animate makereservation p-5 bg-light">
         
          <?php 
          $form = ActiveForm::begin(['id' => 'login-form']); ?>  
          <form>
              <h2 class="mb-4">Data Penghuni</h2>
              <div class="row mb-5">
                <div class="col-md-6">
                  <div class="form-group">
                    
                    <?= $form->field($penghuni, 'nama_lengkap')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Nama Lengkap']) ?>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  
                  <?= $form->field($penghuni, 'pekerjaan')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Pekerjaan']) ?>
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  
                  <div class="select-wrap one-third">
                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                    <?= $form->field($penghuni, 'jenis_kelamin')->radioList(array('1'=>'Laki-Laki',2=>'Perempuan')); ?>
                  </div>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="form-group">
                   
                    <?= $form->field($penghuni, 'no_hp')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'No Handphone']) ?>
                  </div>
                </div>
                <div class="col-md-6">
              <div class="form-group">
                
                <?= $form->field($penghuni, 'deskripsi')->textarea(['rows' => 6 , 'class' => 'form-control', 'placeholder' => 'Deskripsi Diri']) ?>
              </div>
            </div>
              </div>
              <div class="row mb-5">
                <div class="col-md-12">
                  <h2 class="mb-4">Mau Kost Kapan?</h2>
                </div>
                <div class="col-md-12">
                  <h3 class="h4">Pendataan Booking</h3>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                 
                <?php 
// with an ActiveForm instance 
?>
<?= $form->field($booking, 'tgl_masuk')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
         // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>
                </div>
              </div>   
              <div class="col-md-6">
            <div class="form-group">
              
              <div class="select-wrap one-third">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?= $form->field($booking, 'id_hitungan')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(HitunganSewa::find()->all(), 'id', 'hitungan_sewa'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Pilih Hitungan Sewa', 'required'=>true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

                ?>
              </div>
            </div>
          </div> 
            <div class="col-md-6">
            <div class="form-group">
              
              <div class="select-wrap one-third">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?= $form->field($booking, 'id_durasi')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(DurasiSewa::find()->all(), 'id', 'durasi'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Pilih Durasi Sewa', 'required'=>true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

                ?>
              </div>
            </div>
          </div>
              
              </div>
              <div class="form-group">
                    <?= Html::submitButton('Booking', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'login-button']) ?>
                </div>
                </form>
            <?php ActiveForm::end(); ?>
            
          </div>
        </div>
      </div>
    </section>
