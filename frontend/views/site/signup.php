<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="site-signup">
<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Masuk</span></p>
              <h1 class="mb-3">Daftar</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ftco-section contact-section">
    <div class="container bg-light">
      <div class="row d-flex mb-5 contact-info">
        <div class="col-md-12 mb-4">
          <h2 class="h4">Daftar</h2>
        </div>
        <div class="w-100"></div>
        <div class="col-md-6">
          <p>Tolong isi Form berikut untuk Mendaftarkan Diri</p>
        </div>
        <div class="col-md-6">
          <p><span>Jika Anda sudah mempunyai Akun Ana Kos</span></p><a href="<?= Url::toRoute(['site/login']); ?>">Silahkan Masuk disini</a>
        </div>
      </div>
      <div class="row block-9">
        <div class="col-md-6 pr-md-5">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?> 
        <form>
            <div class="form-group">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control']) ?>
            </div>
            <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['class' => 'form-control']) ?>
            </div>
            
            <div class="form-group">
            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'signup-button']) ?>
                </div>
          </form>
          <?php ActiveForm::end(); ?>
        </div>

        <div class="img col-sm-12 col-lg-6 order-last" style="background-image: url('../images/about-2.jpg');">
        </div>
      </div>
    </div>
  </section>