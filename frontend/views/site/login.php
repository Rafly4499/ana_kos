<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="site-login">
<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Masuk</span></p>
              <h1 class="mb-3">Masuk</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section contact-section">
      <div class="container bg-light">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">Masuk</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-6">
            <p>Tolong isi Form berikut untuk Masuk</p>
          </div>
          <div class="col-md-6">
            <p><span>Jika Anda belum mempunyai Akun Ana Kos</span></p><a href="<?= Url::toRoute(['site/signup']); ?>">Silahkan Daftar disini</a>
          </div>
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
          <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>  
          <form>
              <div class="form-group">
              <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control']) ?>
              </div>
              <div class="form-group">
              <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control']) ?>
              </div>
              <div class="form-group">
              <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                    <br>
                    Need new verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
                </div>
              </div>
              <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'login-button']) ?>
                </div>
            </form>
            <?php ActiveForm::end(); ?>
          </div>

          <div class="img col-sm-12 col-lg-6 order-last" style="background-image: url('../images/about-2.jpg');">
          </div>
        </div>
      </div>
    </section>
   