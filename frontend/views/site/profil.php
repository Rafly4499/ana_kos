<?php
      use yii\helpers\Html;
      use yii\helpers\Url;
?>
<section class="home-slider owl-carousel">
<div class="slider-item" style="background-image: url('../../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row slider-text align-items-end">
      <div class="col-md-10 col-sm-12 ftco-animate mb-4">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Profil</span></p>
        <h1 class="mb-3">Profil</h1>
      </div>
    </div>
  </div>
</div>
</section>

<section class="ftco-section">
<div class="container">
  <div class="row">
    <div class="col-md-6 ftco-animate">
      <h2 class="mb-3">Profile</h2>
      <h6>Username</h6>
      <h5><?= $profile->username ?></h5><br>
    
     <h6>Email</h6>
     <h5><?= $profile->email ?></h5><br><br>
      
     <h3 class="mb-3">Data Penghuni</h3>
     <h6>Nama Lengkap</h6>
      <h5><?= $penghuni->nama_lengkap ?></h5><br>
      <h6>Jenis Kelamin</h6>
      <h5><?php if($penghuni->jenis_kelamin == 1){
                   echo 'Laki - Laki';
                }else{
                    echo 'Perempuan';
                } ?> </h5>
      <h6>No Handphone</h6>
      <h5><?= $penghuni->no_hp ?></h5><br>
      <h6>Pekerjaan</h6>
      <h5><?= $penghuni->pekerjaan ?></h5><br>

    </div> <!-- .col-md-8 -->
    <div class="col-md-6 sidebar">
    <div class="sidebar-box ftco-animate">
        <div class="categories">
        <h2 class="mb-3">Histori</h2>
          <p>Histori Data Booking Anda</p>
          <?php
              if ($histori) {
              foreach ($histori as $historibook => $resulthistori) {
              ?>
          <div class="jumbotron2 histori"><h4><?= $resulthistori->kost->nama_kost ?></h4>
          <a href="" class="historiimg">
          <?= Html::img(Yii::$app->urlManagerBackend->baseUrl . '/images/' . $resulthistori->kost->gambar) ?><br>
          </a>
          <p>Tanggal Masuk : <?= $resulthistori->tgl_masuk ?></p>
                <p>Status : 
                    <?php 
                    if($resulthistori->status == 0){
                    echo 'Belum Dikonfirmasi';
                }else{
                    echo 'Sudah Dikonfirmasi';
                } ?></p>
       </div>
          <?php
              }
          }
         ?>
        </div>
      </div>
      <div class="sidebar-box ftco-animate">
        <h3>Ana Kos</h3>
        <p>Platform digital penyedia Informasi dan Booking tempat tinggal bagi Anak Kost</p>
      </div>
    </div>

  </div>
</div>
</section>