<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Artikel</span></p>
              <h1 class="mb-3">Artikel</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="" class="block-20" style="background-image: url('../images/image_1.jpg');">
              </a>
              <div class="text p-4 d-block">
                <h3 class="heading"><a href="#">Informasi Lowongan Pekerjaan Bagi Anak Kost</a></h3>
                <div class="meta">
                  <div><a href="#">July 12, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="100">
              <a href="" class="block-20" style="background-image: url('../images/image_2.jpg');">
              </a>
              <div class="text p-4">
                <h3 class="heading"><a href="#">List Kos Ternyaman bagi anak Kost</a></h3>
                <div class="meta">
                  <div><a href="#">Januari 12, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="200">
              <a href="" class="block-20" style="background-image: url('../images/image_3.jpg');">
              </a>
              <div class="text p-4">
                <h3 class="heading"><a href="#">Promo Ana Kos diskon 50%</a></h3>
                <div class="meta">
                  <div><a href="#">November 12, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="200">
              <a href="" class="block-20" style="background-image: url('../images/image_4.jpg');">
              </a>
              <div class="text p-4">
                <h3 class="heading"><a href="#">Cara Anak Kost untuk meningkatkan produktivitas</a></h3>
                <div class="meta">
                  <div><a href="#">Desember 10, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="200">
              <a href="" class="block-20" style="background-image: url('../images/image_5.jpg');">
              </a>
              <div class="text p-4">
                <h3 class="heading"><a href="#">Informasi Jadi Mitra Anak Kost</a></h3>
                <div class="meta">
                  <div><a href="#">April 12, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry" data-aos-delay="200">
              <a href="" class="block-20" style="background-image: url('../images/image_6.jpg');">
              </a>
              <div class="text p-4">
                <h3 class="heading"><a href="#">10 tipe Anak Kost</a></h3>
                <div class="meta">
                  <div><a href="#">July 5, 2019</a></div>
                  <div><a href="#">Admin</a></div>
                  <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>