<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
  use yii\bootstrap\ActiveForm;
?>
<section class="home-slider owl-carousel">
<div class="slider-item" style="background-image: url('../images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row slider-text align-items-center justify-content-start">
      <div class="col-md-6 col-sm-12 ftco-animate">
        <h1 class="mb-4">Selamat Datang di Ana Kos, Platform penyedia tempat tinggal untuk Anak Kos</h1>
        <p><a href="https://vimeo.com/45830194" class="btn btn-primary btn-outline-white px-4 py-3 popup-vimeo"><span class="ion-ios-play mr-2"></span> Watch Video</a></p>
      </div>
    </div>
  </div>
</div>

<div class="slider-item" style="background-image: url('../images/bg_2.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row slider-text align-items-center justify-content-start">
      <div class="col-md-6 col-sm-12 ftco-animate">
        <h1 class="mb-4">Booking sesuai dengan keinginan anda</h1>
        <p><a href="https://vimeo.com/45830194" class="btn btn-primary btn-outline-white px-4 py-3 popup-vimeo"><span class="ion-ios-play mr-2"></span> Watch Video</a></p>
      </div>
    </div>
  </div>
</div>

<div class="slider-item" style="background-image: url('../images/bg_3.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row slider-text align-items-center justify-content-start">
      <div class="col-md-6 col-sm-12 ftco-animate">
        <h1 class="mb-4">Cari hunian nyaman sesuai dengan budget Anak Kos</h1>
        <p><a href="https://vimeo.com/45830194" class="btn btn-primary btn-outline-white px-4 py-3 popup-vimeo"><span class="ion-ios-play mr-2"></span> Watch Video</a></p>
      </div>
    </div>
  </div>
</div>
</section>
<!-- END slider -->

<div class="ftco-section-reservation">
<div class="container">
  <div class="row justify-content-end ftco-animate">
    <div class="col-lg-4 col-md-5 reservation p-md-5">
      <div class="block-17">
      <?php $form = ActiveForm::begin(['id' => 'form-search',
                                    'action' => ['site/kostsearch'],
                                    'class' => ['d-block'],
                                    'method' => 'get',
                                    'options' => ['method' => 'get'],
                                ]); ?>
       
          <div class="fields d-block">

            <!-- <div class="book-date one-third">
              <label for="check-in">Check in:</label>
              <input type="text" id="checkin_date" class="form-control" placeholder="M/D/YYYY">
            </div>

            <div class="book-date one-third">
              <label for="check-out">Check out:</label>
              <input type="text" id="checkout_date" class="form-control" placeholder="M/D/YYYY">
            </div> -->
            <div class="book-date one-third">
              <label for="check-out">Pencarian Ana Kos</label>
              <input type="text" class="form-control" name="qsearch" placeholder="Cari sesuai dengan Keinginan Anda">
            </div> 
            <div class="one-third">
              <label for="Guest">Kota</label>
              <div class="select-wrap">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?=Html::dropDownList('nama_kota', $selection= NULL, $datalistkota,['prompt'=>'Pilih Kota', 'class' => 'form-control']);?>
              </div>
            </div>
            <div class="one-third">
              <label for="Guest">Filter</label>
              <div class="select-wrap">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?=Html::dropDownList('nama_kategori', $selection= NULL, $datalist,['prompt'=>'Pilih Kategori Kos', 'class' => 'form-control']);?>
              </div>
            </div>
          </div>
          <?= Html::submitButton('Cari', ['class' => 'search-submit btn btn-primary', 'name' => 'signup-button']) ?> 
          <?php $form = ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
</div>

<section class="services bg-light">
<div class="container">
  <div class="row no-gutters">
    <div class="col-md-4 ftco-animate py-5 nav-link-wrap aside-stretch">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link px-4 active" id="v-pills-master-tab" data-toggle="pill" href="#v-pills-master" role="tab" aria-controls="v-pills-master" aria-selected="true"><span class="mr-3 flaticon-bed"></span>Kamar Kos</a>

        <a class="nav-link px-4" id="v-pills-buffet-tab" data-toggle="pill" href="#v-pills-buffet" role="tab" aria-controls="v-pills-buffet" aria-selected="false"><span class="mr-3 flaticon-tray"></span>Apartemen</a>

        <a class="nav-link px-4" id="v-pills-fitness-tab" data-toggle="pill" href="#v-pills-fitness" role="tab" aria-controls="v-pills-fitness" aria-selected="false"><span class="mr-3 flaticon-woman"></span>Rumah Kontrak</a>
      </div>
    </div>
    <div class="col-md-8 ftco-animate p-4 p-md-5 d-flex align-items-center">
      
      <div class="tab-content pl-md-5" id="v-pills-tabContent">

        <div class="tab-pane fade show active" id="v-pills-master" role="tabpanel" aria-labelledby="v-pills-master-tab">
          <span class="icon mb-3 d-block flaticon-bed"></span>
          <h2 class="mb-2">Kamar Kos</h2>
          <p class="lead">Menyediakan berbagai macam tipe kamar kos untuk Anak Kost Putra, Putri dan Campur</p>
          <p><a href="<?= Url::toRoute(['site/fasilitas', 'id' => 2]); ?>" class="btn btn-primary">Lihat Kamar Kos</a></p>
        </div>

        <div class="tab-pane fade" id="v-pills-buffet" role="tabpanel" aria-labelledby="v-pills-buffet-tab">
          <span class="icon mb-3 d-block flaticon-tray"></span><h2 class="mb-2">Apartemen</h2>
          
          <p class="lead">Menyediakan Berbagai macam tipe apartemen untuk Anak Kost</p>
          <p><a href="<?= Url::toRoute(['site/fasilitas', 'id' => 3]); ?>" class="btn btn-primary">Lihat Apartemen</a></p>
        </div>

        <div class="tab-pane fade" id="v-pills-fitness" role="tabpanel" aria-labelledby="v-pills-fitness-tab">
          <span class="icon mb-3 d-block flaticon-woman"></span>
          <h2 class="mb-2">Rumah Kontrak</h2>
          <p class="lead">Menyediakan informasi mengenai rumah kontrak dalam harga yang terjangkau</p>
          <p><a href="<?= Url::toRoute(['site/fasilitas', 'id' => 4]); ?>" class="btn btn-primary">Lihat Rumah Kontrak</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<section class="ftco-section bg-light">
<div class="container">
  <div class="row justify-content-center mb-5 pb-5">
    <div class="col-md-7 text-center heading-section ftco-animate">
    
      <span class="subheading">Kota Besar</span>
      <h2> Cari Kos, Apartemen, Rumah Kontrak Sesuai dengan Kota Anda</h2>
    </div>
  </div>
  <div class="row">
  <div class="col-md-12 ftco-animate">
  <?php
    if ($kota) {
      foreach ($kota as $datakota => $resultkota) {

    ?>
      <div class="jumbotron col-md-3 kota">
      <a href="<?= Url::toRoute(['site/kota', 'id' => $resultkota->id]); ?>">
       
        <?= Html::img(Yii::$app->urlManagerBackend->baseUrl . '/images/' . $resultkota->image) ?>
      <h3 class="text-center headkota"><?= $resultkota->nama_kota ?></h3>
      </a>
  </div>
     <?php
      }
    }
     ?>
    </div>
  </div>
</div>
</section>
<section class="ftco-section room-section">
<div class="container">
  <div class="row justify-content-center mb-5 pb-5">
    <div class="col-md-7 text-center heading-section ftco-animate">
      <span class="subheading">Kamar Kos Terfavorit</span>
      <h2>Cari Sesuai dengan keinginan Anda</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 ftco-animate">
     <div class="carousel-room owl-carousel">
     <?php
    if ($kost) {
      foreach ($kost as $datakost => $resultkost) {

    ?>
      <div class="item">
        <div class="room-wrap">
          <a href="#" class="room-img"><?= Html::img(Yii::$app->urlManagerBackend->baseUrl . '/images/' . $resultkost->gambar) ?></a>
          <div class="text p-4">
            <div class="d-flex mb-1">
              <div class="one-third">
                <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                <h3><a href="#"><?= $resultkost->nama_kost ?></a></h3>
              </div>
              <div class="one-forth text-center">
                <p class="price"><?= $resultkost->harga ?><br><span>/Bulanan</span></p>
              </div>
            </div>  
            <p class="features">
              <span class="d-block mb-2"><i class="icon-check mr-2"></i> Kasur</span>
              <span class="d-block mb-2"><i class="icon-check mr-2"></i> Lemari Pakaian</span>
              <span class="d-block mb-2"><i class="icon-check mr-2"></i> Wifi Internet</span>
              <span class="d-block mb-2"><i class="icon-check mr-2"></i> Kamar Mandi</span>
              <span class="d-block mb-2"><i class="icon-check mr-2"></i> Free wifi</span>
            </p>
            <p><a href="<?= Url::toRoute(['site/detailkost', 'id' => $resultkost->id]); ?>" class="btn btn-primary">Booking</a></p>
          </div>
        </div>
      </div>
      <?php
      }
    }
     ?>
     </div>
    </div>
  </div>
</div>
</section>
<section class="ftco-section-parallax">
<div class="parallax-img d-flex align-items-md-center align-items-sm-end" style="background-image: url('../images/bg_4.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row desc d-flex justify-content-center">
      <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
        <span class="subheading">Profil Kami</span>
        <h2 class="h1 font-weight-bold">Company Profile</h2>
        <p><a href="#" class="btn btn-primary btn-outline-white mt-3 py-3 px-4">View more details</a></p>
      </div>
    </div>
  </div>
</div>
</section>
<section class="ftco-section testimony-section">
<div class="container">
  <div class="row justify-content-center mb-5 pb-5">
    <div class="col-md-7 text-center heading-section ftco-animate">
      <span class="subheading">Testimoni</span>
      <h2>Testimoni Anak Kost</h2>
    </div>
  </div>
  <div class="row ftco-animate">
    <div class="col-md-12">
      <div class="carousel owl-carousel ftco-owl">
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Mantapp jiwaa... Ana Kos Kerenn</p>
              <p class="name">Nadhif</p>
              <span class="position">Anak Kos From Surabaya</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_2.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Bermanfaat sekali bambanggg... saya suka saya suka. Pencarian mudah dan cepat</p>
              <p class="name">Budi Santoso</p>
              <span class="position">Anak Kost From Bandung</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_3.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Layanan mempermudah para kaum anak kos. semoga kedepannya dapat dikembangkan lebih baik</p>
              <p class="name">Rafly Arief Kanza</p>
              <span class="position">Anak Kost Form Sidoarjo</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Efektif dan efisien dalam pencarian kos. Sangatt mantull sekali</p>
              <p class="name">Bambang Marhambang</p>
              <span class="position">Anak Kost From Jakarta</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Bermanfaat bagi kaum miskin wkwk..</p>
              <p class="name">Stevanus Farhandi</p>
              <span class="position">Anak Kost From Malang</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<section class="ftco-section bg-light">
<div class="container">
  <div class="row justify-content-center mb-5 pb-5">
    <div class="col-md-7 text-center heading-section ftco-animate">
      <span class="subheading">Artikel</span>
      <h2>Artikel Terbaru</h2>
    </div>
  </div>
  <div class="row ftco-animate">
    <div class="carousel1 owl-carousel ftco-owl">
      <div class="item">
        <div class="blog-entry">
          <a href="blog-single.html" class="block-20" style="background-image: url('../images/image_5.jpg');">
          </a>
          <div class="text p-4 d-block">
            <h3 class="heading"><a href="#">List Kos Ternyaman bagi anak Kost</a></h3>
            <div class="meta">
              <div><a href="#">July 12, 2019</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="blog-entry" data-aos-delay="100">
          <a href="blog-single.html" class="block-20" style="background-image: url('../images/image_6.jpg');">
          </a>
          <div class="text p-4">
            <h3 class="heading"><a href="#">Promo Ana Kos diskon 50%</a></h3>
            <div class="meta">
              <div><a href="#">November 12, 2019</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="blog-entry" data-aos-delay="200">
          <a href="blog-single.html" class="block-20" style="background-image: url('../images/image_7.jpg');">
          </a>
          <div class="text p-4">
            <h3 class="heading"><a href="#">Informasi Lowongan Pekerjaan bagi Anak Kos</a></h3>
            <div class="meta">
              <div><a href="#">Desember 1, 2019</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
          </div>
        </div>
      </div>
      

    </div>
  </div>
</div>
</section>
