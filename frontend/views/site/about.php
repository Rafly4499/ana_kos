<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<section class="home-slider owl-carousel">
<div class="slider-item" style="background-image: url('../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
  <div class="overlay"></div>
  <div class="container">
    <div class="row slider-text align-items-end">
      <div class="col-md-10 col-sm-12 ftco-animate mb-4">
        <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Tentang Kami</span></p>
        <h1 class="mb-3">Tentang Kami</h1>
      </div>
    </div>
  </div>
</div>
</section>

<section class="ftco-section-2">
<div class="container d-flex">
  <div class="section-2-blocks-wrapper row d-flex">
    <div class="img col-sm-12 col-lg-6 order-last" style="background-image: url('../images/about-2.jpg');">
    </div>
    <div class="text col-lg-6 order-first ftco-animate">
      <div class="text-inner align-self-start">
        <span class="subheading">Tentang Ana Kos</span>
        <h3 class="heading">Selamat Datang Di Ana Kos</h3>
        <p>Berawal dari keresahan para Anak Kos yang mencari tempat tinggal yang susah dan para pemilik kos bermasalah dalam memasarkan usaha mereka</p>

        <p>Maka dari itu kami memberikan solusi berupa Ana Kos. Ana Kos merupakan platform digital pencarian tempat tinggal bagi Anak Kos berbasis aplikasi dan kemitraan</p>
        <p>Ana Kos menawarkan beberapa layanan fitur untuk mempermudah para anak kost dalam mencari tempat tinggal dan para pemilik kos untuk mengelola usaha mereka</p>  
    </div>
    </div>
  </div>
</div>
</section>

<section class="ftco-section testimony-section">
<div class="container">
  <div class="row justify-content-center mb-5 pb-5">
    <div class="col-md-7 text-center heading-section ftco-animate">
      <span class="subheading">Testimoni</span>
      <h2>Testimoni Anak Kost</h2>
    </div>
  </div>
  <div class="row ftco-animate">
    <div class="col-md-12">
      <div class="carousel owl-carousel ftco-owl">
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Mantapp jiwaa... Ana Kos Kerenn</p>
              <p class="name">Nadhif</p>
              <span class="position">Anak Kos From Surabaya</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_2.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Bermanfaat sekali bambanggg... saya suka saya suka. Pencarian mudah dan cepat</p>
              <p class="name">Budi Santoso</p>
              <span class="position">Anak Kost From Bandung</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_3.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Layanan mempermudah para kaum anak kos. semoga kedepannya dapat dikembangkan lebih baik</p>
              <p class="name">Rafly Arief Kanza</p>
              <span class="position">Anak Kost Form Sidoarjo</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Efektif dan efisien dalam pencarian kos. Sangatt mantull sekali</p>
              <p class="name">Bambang Marhambang</p>
              <span class="position">Anak Kost From Jakarta</span>
            </div>
          </div>
        </div>
        <div class="item text-center">
          <div class="testimony-wrap p-4 pb-5">
            <div class="user-img mb-4" style="background-image: url(../images/person_1.jpg)">
              <span class="quote d-flex align-items-center justify-content-center">
                <i class="icon-quote-left"></i>
              </span>
            </div>
            <div class="text">
              <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
              <p class="mb-5">Bermanfaat bagi kaum miskin wkwk..</p>
              <p class="name">Stevanus Farhandi</p>
              <span class="position">Anak Kost From Malang</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
