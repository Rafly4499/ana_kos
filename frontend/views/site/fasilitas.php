<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
  use yii\bootstrap\ActiveForm;
?>
<section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('../../images/bg_1.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-end">
            <div class="col-md-10 col-sm-12 ftco-animate mb-4">
              <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Layanan Kos</span></p>
              <h1 class="mb-3">Layanan Kos</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="ftco-section-reservation">
<div class="container">
  <div class="row justify-content-end ftco-animate">
    <div class="col-lg-4 col-md-5 reservation p-md-5">
      <div class="block-17">
      <?php $form = ActiveForm::begin(['id' => 'form-search',
                                    'action' => ['site/kostsearch'],
                                    'class' => ['d-block'],
                                    'method' => 'get',
                                    'options' => ['method' => 'get'],
                                ]); ?>
       
          <div class="fields d-block">

            <!-- <div class="book-date one-third">
              <label for="check-in">Check in:</label>
              <input type="text" id="checkin_date" class="form-control" placeholder="M/D/YYYY">
            </div>

            <div class="book-date one-third">
              <label for="check-out">Check out:</label>
              <input type="text" id="checkout_date" class="form-control" placeholder="M/D/YYYY">
            </div> -->
            <div class="book-date one-third">
              <label for="check-out">Pencarian Ana Kos</label>
              <input type="text" class="form-control" name="qsearch" placeholder="Cari sesuai dengan Keinginan Anda">
            </div> 
            <div class="one-third">
              <label for="Guest">Kota</label>
              <div class="select-wrap">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?=Html::dropDownList('nama_kota', $selection= NULL, $datalistkota,['prompt'=>'Pilih Kota', 'class' => 'form-control']);?>
              </div>
            </div>
            <div class="one-third">
              <label for="Guest">Filter</label>
              <div class="select-wrap">
                <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                <?=Html::dropDownList('nama_kategori', $selection= NULL, $datalist,['prompt'=>'Pilih Kategori Kos', 'class' => 'form-control']);?>
              </div>
            </div>
          </div>
          <?= Html::submitButton('Cari', ['class' => 'search-submit btn btn-primary', 'name' => 'signup-button']) ?> 
          <?php $form = ActiveForm::end(); ?> 
      </div>
    </div>
  </div>
</div>
</div>

    <section class="ftco-section room-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <span class="subheading">Layanan Kos</span>
            <h2>Informasi dan Booking tempat tinggal sesuai dengan keinginan Anda</h2>
            <?php
                if($kost == Null){
                ?>

<h3>Tidak Ada Data. Silahkan Cek Pencarian Anda</h3>
                <?php
                }
                ?>
          </div>
        </div>
        <div class="row">
      
        <?php
      foreach ($kost as $datakost => $resultkost) {

    ?>
          <div class="col-md-4 ftco-animate">
            <div class="room-wrap">
              <a href="#" class="room-img"><?= Html::img(Yii::$app->urlManagerBackend->baseUrl . '/images/' . $resultkost->gambar) ?></a>
              <div class="text p-4">
                <div class="d-flex mb-1">
                  <div class="one-third">
                    <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span></p>
                    <h3><a href="#"><?= $resultkost->nama_kost ?></a></h3>
                  </div>
                  <div class="one-forth text-center">
                    <p class="price"><?= $resultkost->harga ?><br><span>/Bulanan</span></p>
                  </div>
                </div>
                <p class="features">
                <span class="d-block mb-2"><i class="icon-check mr-2"></i> Kasur</span>
                <span class="d-block mb-2"><i class="icon-check mr-2"></i> Lemari Pakaian</span>
                <span class="d-block mb-2"><i class="icon-check mr-2"></i> Wifi Internet</span>
                <span class="d-block mb-2"><i class="icon-check mr-2"></i> Kamar Mandi</span>
                <span class="d-block mb-2"><i class="icon-check mr-2"></i> Free wifi</span>
                </p>
                <p><a href="<?= Url::toRoute(['site/detailkost', 'id' => $resultkost->id]); ?>" class="btn btn-primary">Booking</a></p>
              </div>
            </div>
          </div>
          <?php
      }
     ?>
        </div>
      </div>
    </section>