-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2019 at 02:18 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ana_kost`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(10) NOT NULL,
  `id_user` int(10) DEFAULT NULL,
  `id_kost` int(10) DEFAULT NULL,
  `id_penghuni` int(10) DEFAULT NULL,
  `id_hitungan` int(10) DEFAULT NULL,
  `id_durasi` int(10) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `id_user`, `id_kost`, `id_penghuni`, `id_hitungan`, `id_durasi`, `tgl_masuk`, `status`, `created_time`, `modified_time`) VALUES
(1, 1, 1, 1, 1, 3, '2019-12-05', 0, '2019-11-29 17:00:00', '2019-11-29 17:00:00'),
(2, 2, 12, 4, 1, 3, '0000-00-00', 0, NULL, NULL),
(3, 2, 12, 5, 1, 3, '0000-00-00', 1, NULL, NULL),
(4, 2, 12, 6, 1, 3, '2019-11-15', 1, NULL, NULL),
(5, 2, 12, 7, 1, 3, '2019-12-07', 1, NULL, NULL),
(6, 2, 12, 8, 2, 4, '2019-11-28', 1, NULL, NULL),
(7, 2, 12, 9, 1, 3, '2019-12-07', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `durasi_sewa`
--

CREATE TABLE `durasi_sewa` (
  `id` int(10) NOT NULL,
  `durasi` varchar(50) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `durasi_sewa`
--

INSERT INTO `durasi_sewa` (`id`, `durasi`, `created_time`, `modified_time`) VALUES
(3, '1 Bulan', NULL, NULL),
(4, '2 Bulan', NULL, NULL),
(5, '3 Bulan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hitungan_sewa`
--

CREATE TABLE `hitungan_sewa` (
  `id` int(10) NOT NULL,
  `hitungan_sewa` varchar(50) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hitungan_sewa`
--

INSERT INTO `hitungan_sewa` (`id`, `hitungan_sewa`, `created_time`, `modified_time`) VALUES
(1, 'Bulanan', NULL, NULL),
(2, 'Harian', NULL, NULL),
(3, 'Mingguan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_kost`
--

CREATE TABLE `kategori_kost` (
  `id` int(10) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori_kost`
--

INSERT INTO `kategori_kost` (`id`, `nama_kategori`, `created_time`, `modified_time`) VALUES
(2, 'Kamar Kos', NULL, NULL),
(3, 'Apartemen', NULL, NULL),
(4, 'Rumah Kontrak', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kost`
--

CREATE TABLE `kost` (
  `id` int(10) NOT NULL,
  `id_tipe` int(10) DEFAULT NULL,
  `id_kategori` int(10) DEFAULT NULL,
  `id_kota` int(10) DEFAULT NULL,
  `nama_kost` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `luas_kamar` varchar(50) DEFAULT NULL,
  `fasilitas` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `lokasi` text DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kost`
--

INSERT INTO `kost` (`id`, `id_tipe`, `id_kategori`, `id_kota`, `nama_kost`, `deskripsi`, `luas_kamar`, `fasilitas`, `harga`, `lokasi`, `gambar`, `created_time`, `modified_time`) VALUES
(1, 2, 2, 2, 'Kos Ibu Rohmah', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '2x 5 m', 'Kasur, Lemari, Parkir, Kamar Mandi Luar, Wifi', '700000', 'Jl. Bendo 2 no 5 Malang', 'gambar-1-1576121494.jpg', NULL, '2019-12-12 03:31:34'),
(2, 3, 3, 1, 'Apartemen Matahari', 'LANTAI 8, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '3x5', 'Kasur, Lemari, Parkir, Kamar Mandi Luar, Wifi', '1500000', 'Jl. Rungkut 1 Industri Surabaya', 'gambar-2-1576121598.jpg', NULL, '2019-12-12 03:33:18'),
(3, 3, 3, 3, 'Apartemen Mentari', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '3x5', 'Wifi, Kamar, Kulkas, Lemari', '500000', 'Jl. A Yani 5 ', 'gambar-3-1576121732.jpg', NULL, '2019-12-12 03:35:32'),
(4, 3, 3, 1, 'Kos Ibu Risa', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '3x5', 'Wifi, Kamar, Kulkas, Lemari', '300000', 'tergdfgdfsg', 'gambar-4-1576122204.jpg', NULL, '2019-12-12 03:43:24'),
(5, 1, 2, 1, 'Kos Janji Jiwa', 'Terdapat banyak sekali fasilitas', '9 x 9 m', 'parkir, kamar mandi, kasur, lemari', '600000', 'Jl. Mulyosari 3 Surabaya', 'gambar-5-1576122223.jpg', NULL, '2019-12-12 03:43:43'),
(6, 2, 2, 1, 'Kos Bapak Muji', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '9 x 9 m', 'parkir, kamar mandi, kasur, lemari', '600000', 'Jl. Surabaya A Yani', 'gambar-6-1576122265.jpg', NULL, '2019-12-12 03:44:25'),
(7, 2, 2, 1, 'Kos  Pak Budi', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '9 x 9 m', 'parkir, kamar mandi, kasur, lemari', '600000', 'Jl. Surabaya Hari', 'gambar-7-1576122426.jpg', NULL, '2019-12-12 03:47:06'),
(12, 1, 2, 1, 'Kos Janji Jiwa', 'LANTAI 1, Lokasi sangat strategis dekat mau kemanapun, nyaman, tidak bising, free wifi dan fasilitas parkir luas. Silahkan hubungi pemilik kos untuk fast respon Cp : 081235100989', '5x5 m', 'parkir, kamar mandi, kasur, lemari', '700000', 'Jl. Gebang kidul 5 Surabaya', 'image--1575740010.jpg', '2019-12-07 17:33:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(10) NOT NULL,
  `nama_kota` varchar(50) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `nama_kota`, `image`, `created_time`, `modified_time`) VALUES
(1, 'Surabaya', 'image-1-1575694949.png', NULL, '2019-12-07 05:02:29'),
(2, 'Malang', 'image-2-1575694939.png', NULL, '2019-12-07 05:02:19'),
(3, 'Jabodetabek', 'image-3-1575694925.png', NULL, '2019-12-07 05:02:05'),
(4, 'Makassar', 'image--1575694565.png', '2019-12-06 22:56:05', NULL),
(5, 'Bali', 'image--1575694639.png', '2019-12-06 22:57:19', NULL),
(6, 'Bandung', 'image--1575694659.png', '2019-12-06 22:57:39', NULL),
(7, 'Jogjakarta', 'image--1575694688.png', '2019-12-06 22:58:08', NULL),
(8, 'Medan', 'image--1575694709.png', '2019-12-06 22:58:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1574497567),
('m130524_201442_init', 1574497570),
('m190124_110200_add_verification_token_column_to_user_table', 1574497570);

-- --------------------------------------------------------

--
-- Table structure for table `penghuni`
--

CREATE TABLE `penghuni` (
  `id` int(10) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `jenis_kelamin` int(2) DEFAULT NULL COMMENT '1 Laki-laki; 2 Perempuan',
  `no_hp` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penghuni`
--

INSERT INTO `penghuni` (`id`, `nama_lengkap`, `jenis_kelamin`, `no_hp`, `pekerjaan`, `deskripsi`, `created_time`, `modified_time`) VALUES
(1, 'hoho', 1, '0797686', 'hfdh', 'ytryrety', NULL, NULL),
(4, 'rafly arief kanza', 1, '3242134', 'hfdh', 'sadfadsfasd', NULL, NULL),
(5, 'rafly arief kanza', 1, '3242134', 'hfdh', 'sadfadsfasd', NULL, NULL),
(6, 'Sausan Tsabitah Arif', 2, '08564564', 'Mahasiswa', 'dsfsdfs', NULL, NULL),
(7, 'Budi Doremi', 1, '0836346434', 'Dosen', 'sfsdfsda', NULL, NULL),
(8, 'Sausan Tsabitah Arif', 1, '34234', 'sad', 'klajsdkla', NULL, NULL),
(9, 'Nadhif', 1, '09423423423', 'Pengusaha', 'Orang ganteng', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipe_kost`
--

CREATE TABLE `tipe_kost` (
  `id` int(10) NOT NULL,
  `nama_tipe` varchar(50) NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `modified_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tipe_kost`
--

INSERT INTO `tipe_kost` (`id`, `nama_tipe`, `created_time`, `modified_time`) VALUES
(1, 'Putra', NULL, NULL),
(2, 'Putri', NULL, NULL),
(3, 'Campur', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'rafly', 'TKtz0LsqtPBvu-wLQLP-ZKq_TTuihqha', '$2y$13$wRKZAHEQIj.knlPnB2/zdukwMYq0xpDkT5Gh9//GND4RbkrYEhLjO', NULL, 'rafly4499@gmail.com', 10, 1574497594, 1574497594, 'hH_9FLgGkev2r6N27Srw_JERQrsIttNn_1574497594'),
(2, 'kanza', 'qEPGHb7Ax2-Ud_xsKxvrVr_Ka125Tods', '$2y$13$fmHCrN8ioLlD9KbxSFN4gO9dLo8A5LjL5edK3JLP5DZ1GafkA12Zq', NULL, 'kanza@gmail.com', 10, 1575704885, 1575704885, 'O2652--TDEU_nvve0xrQf0w-PQ1MJn9f_1575704885');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_durasi` (`id_durasi`),
  ADD KEY `id_hitungan` (`id_hitungan`),
  ADD KEY `id_penghuni` (`id_penghuni`),
  ADD KEY `id_kost` (`id_kost`);

--
-- Indexes for table `durasi_sewa`
--
ALTER TABLE `durasi_sewa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hitungan_sewa`
--
ALTER TABLE `hitungan_sewa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_kost`
--
ALTER TABLE `kategori_kost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kost`
--
ALTER TABLE `kost`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_kota` (`id_kota`),
  ADD KEY `id_tipe` (`id_tipe`) USING BTREE;

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `penghuni`
--
ALTER TABLE `penghuni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_kost`
--
ALTER TABLE `tipe_kost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `durasi_sewa`
--
ALTER TABLE `durasi_sewa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hitungan_sewa`
--
ALTER TABLE `hitungan_sewa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kategori_kost`
--
ALTER TABLE `kategori_kost`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kost`
--
ALTER TABLE `kost`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penghuni`
--
ALTER TABLE `penghuni`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tipe_kost`
--
ALTER TABLE `tipe_kost`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
